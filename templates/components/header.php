<?php

use Backstage\SetDesign\Message\MessageView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Orchestrator\GlobalOptions;

?>
<?php if (GlobalOptions::enableMessaging()): ?>
    <?= MessageView::createGlobal(); ?>
<?php endif; ?>
<header class="header-nav sticky-header" data-gtm="Header">
    <?php if (has_nav_menu('primary_navigation')): ?>
        <div class="header-nav__menu">
            <?= NavMenuView::create('primary_navigation'); ?>
        </div>
    <?php endif; ?>
    <div class="header-nav__brand">
        <a class="header-nav__brand-link" href="<?= home_url() ?>">
            <?php if ($header_image = GlobalOptions::headerBrandImage()): ?>
            <?= $header_image->css_class('header-nav__brand-image') ?>
            <?php else: ?>
            <strong><?php bloginfo('name'); ?></strong>
            <?php endif; ?>
        </a>
    </div>
    <div class="header-nav__cta">
        <?php
            if ($header_cta_link = GlobalOptions::headerCTALink()) {
                $classes = ['header-nav__cta-link', get_theme_mod('header__cta__link-style', 'button')];
                echo $header_cta_link->class($classes);
            }
        ?>
    </div>
</header>
