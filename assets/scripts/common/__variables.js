/**
 * Global variables within theme scope
 */
var ajaxUrl = window.sit.ajax_url;
var apiUrl = window.sit.api_url;
var doAction = SetDesign.doAction;
var addAction = SetDesign.addAction;
var removeAction = SetDesign.removeAction;
var INIT = SetDesign.INIT;
var LAYOUT = SetDesign.LAYOUT;
var LAYOUTEND = SetDesign.LAYOUTEND;
var SCROLLSTART = SetDesign.SCROLLSTART;
var SCROLL = SetDesign.SCROLL;
var SCROLLEND = SetDesign.SCROLLEND;
var GA_EVENT = SetDesign.GA_EVENT;
